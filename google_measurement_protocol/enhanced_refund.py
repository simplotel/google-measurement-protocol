from .event import event


def enhanced_refund_item(item_id, quantity=None, **extra_data):
    payload = {'id': item_id, 'qt': quantity or 1}
    payload.update(extra_data)
    return payload


def enhanced_refund(transaction_id, items=[], **extra_data):
    for event_item in event('ecommerce', 'refund'):
        yield event_item

    payload = {'pa': 'refund', 'ti': transaction_id}
    payload.update(extra_data)

    for position, item in enumerate(items):
        payload.update(_finalize_enhanced_refund_item(item, position + 1))

    yield payload


def _finalize_enhanced_refund_item(item, position):
    position_prefix = 'pr{0}'.format(position)
    final_item = {}
    for key, value in item.items():
        final_item[position_prefix + key] = value
    return final_item
